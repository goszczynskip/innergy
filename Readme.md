# Warehouse stock data formatter
Cli program for formatting warehouse stock data provided via standard input.

## Running
Build and Run *App* project.

When program isn't run with preset data in stdio stream, than stream termination char 
on line start is needed to make program start parsing. On Windows based systems it's 
`CTRL+Z`. On Linux based systems it's `CTRL+D`.

### Example:
#### Manual input
```
<projectRoot>/App> dotnet run
# This is typed by hand in commandline
Material One;Mat-1;WH-B,5|WH-A,10
^Z
```

#### Piped input
```
<projectRoot>/App> echo "Material One;Mat-1;WH-B,5|WH-A,10" | dotnet run
```


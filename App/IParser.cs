using System.Collections.Generic;
using Core;

namespace App
{
    public interface IParser
    {
        ICollection<IWarehouse> Parse(string input);
    }
}

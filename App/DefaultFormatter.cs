using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace App
{
    public class DefaultFormatter: IFormatter
    {
        public string Format(IEnumerable<IWarehouse> warehouses)
        {
            var stringBuilder = new StringBuilder();
            var orderedWarehouses = warehouses.OrderByDescending(w => w.TotalInStock).ThenBy(w => w.Id);
            foreach (var warehouse in orderedWarehouses)
            {
                stringBuilder.Append($"{warehouse.Id} (total {warehouse.TotalInStock})\n");
                foreach (var (material, stockInformation) in warehouse.GetStock().OrderBy(s => s.Key.Id))
                {
                    stringBuilder.Append($"{material.Id}: {stockInformation.Amount}\n");
                }

                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }
    }
}

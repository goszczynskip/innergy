﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Core;

namespace App
{
    internal static class Program
    {
        private static readonly IParser DefaultParser = new DefaultParser(new DefaultWarehouseFactory(), new DefaultMaterialFactory());
        private static readonly IFormatter DefaultFormatter = new DefaultFormatter();

        private static void Main(string[] args)
        {
           var text = Console.In.ReadToEnd();
           IEnumerable<IWarehouse> warehouses;
           
           try
           {
               warehouses = DefaultParser.Parse(text);
           }
           catch (Exception)
           {
               Console.Error.Write("Unexpected input format");
               Environment.Exit(1);
               return;
           } 
           
           var result = DefaultFormatter.Format(warehouses);
           Console.Out.Write(result);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using Core;

namespace App
{
    public interface IFormatter
    {
        string Format(IEnumerable<IWarehouse> warehouses);
    }
}

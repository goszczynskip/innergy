using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Core;

namespace App
{
    public class DefaultParser: IParser
    {
        private readonly Regex _entryPattern = new Regex(@"(?<MaterialName>.*);(?<MaterialId>.*);(?<WarehousesInfo>(?:.*,.*\|?)+)");
        private readonly Regex _warehousesInfoPattern = new Regex(@"(?<WarehouseId>[\w-]*),(?<Amount>[\w-]*)\|?");
        private readonly IWarehouseFactory _warehouseFactory;
        private readonly IMaterialFactory _materialFactory;

        private const string EntrySplitString = "\n";
        private const string CommentStartString = "#";

        public DefaultParser(IWarehouseFactory warehouseFactory, IMaterialFactory materialFactory)
        {
            _warehouseFactory = warehouseFactory;
            _materialFactory = materialFactory;
        }
        
        public ICollection<IWarehouse> Parse(string input)
        {
            var warehouses = new Dictionary<string, IWarehouse>();
            var entries = input.Split(EntrySplitString);
            
            foreach (var entry in entries)
            {
                if (entry.StartsWith(CommentStartString))
                {
                    continue;
                }

                var match = _entryPattern.Match(entry);

                if (!match.Success)
                {
                    continue;
                }
                
                var materialIdGroup = match.Groups["MaterialId"];
                var materialNameGroup = match.Groups["MaterialName"];
                var material = _materialFactory.Create(
                    materialIdGroup.Value,
                    materialNameGroup.Value
                );
                
                var warehousesInfo = match.Groups["WarehousesInfo"].Value;
                IList<Match> warehousesInfoMatch = _warehousesInfoPattern.Matches(warehousesInfo);

                foreach (var warehouseInfoMatch in warehousesInfoMatch)
                {
                    var warehouseIdGroup = warehouseInfoMatch.Groups["WarehouseId"];
                    var materialAmountGroup = warehouseInfoMatch.Groups["Amount"];
                    
                    if (!warehouses.TryGetValue(warehouseIdGroup.Value, out var warehouse))
                    {
                        warehouse = _warehouseFactory.Create(warehouseIdGroup.Value);
                        warehouses.Add(warehouseIdGroup.Value, warehouse);
                    }
                    
                    warehouse.AddMaterial(material, int.Parse(materialAmountGroup.Value));
                }
            }

            return warehouses.Values;
        }
    }
}

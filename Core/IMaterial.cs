using System;

namespace Core
{
    public interface IMaterial: IEquatable<IMaterial>
    {
        public string Id { get; }
        public string Name { get; }
        
    }
}

using System.Data.Common;

namespace Core
{
    public interface IMaterialFactory
    {
        IMaterial Create(string id, string name);
    }
}

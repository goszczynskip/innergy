namespace Core
{
    public class DefaultWarehouseFactory: IWarehouseFactory
    {
        public IWarehouse Create(string id)
        {
            return new DefaultWarehouse(id);
        }
    }
}

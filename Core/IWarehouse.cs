using System.Collections;
using System.Collections.Generic;

namespace Core
{
    public interface IWarehouse
    {
        string Id { get; }
        void AddMaterial(IMaterial material, int amount);
        IEnumerable<KeyValuePair<IMaterial, StockInformation>> GetStock();
        int TotalInStock { get; }
    }
}

namespace Core
{
    public interface IWarehouseFactory
    {
        IWarehouse Create(string id);
    }
}

namespace Core
{
    public class DefaultMaterialFactory: IMaterialFactory
    {
        public IMaterial Create(string id, string name)
        {
            return new DefaultMaterial(id, name);
        }
    }
}

using System.Collections.Generic;
using System.Linq;

namespace Core
{
    /// <summary>
    /// Default warehouse implementation. NO THREAD SAFETY!
    /// </summary>
    public class DefaultWarehouse: IWarehouse
    {
        private readonly IDictionary<IMaterial, StockInformation> _stock = new Dictionary<IMaterial, StockInformation>();

        public DefaultWarehouse(string id)
        {
            Id = id;
        }
        public string Id { get; }

        public void AddMaterial(IMaterial material, int amount)
        {
            if (_stock.TryGetValue(material, out var stockInformation))
            {
                stockInformation.Amount+=amount;
            }
            else
            {
                _stock.Add(material, new StockInformation()
                {
                    Amount = amount
                });
            }
        }

        public IEnumerable<KeyValuePair<IMaterial, StockInformation>> GetStock()
        {
            return _stock;
        }

        public int TotalInStock
        {
            get
            {
                return _stock.Values.Aggregate(0,(acc, next) => acc + next.Amount);
            }
        }
    }
}

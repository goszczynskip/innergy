using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App;
using Core;
using Xunit;

namespace Test
{
    public class DefaultParsesTests
    {
        public static IEnumerable<object[]> GetSimpleInput()
        {
            yield return new object[]
            {
                "Material One;Mat-1;WH-B,5|WH-A,10\n" +
                "Material Two 2\" -;Mat-2;WH-A,15"
            };
        }

        public static IEnumerable<object[]> GetInputWithComments()
        {
            yield return new object[]
            {
                "# Comment 1\n" +
                "Material One;Mat-1;WH-B,5|WH-A,10\n" +
                "# Comment 2\n" +
                "Material Two 2\" -;Mat-2;WH-A,15"
            };
        }

        public static IEnumerable<object[]> GetInputWithCommentsAndWhitespaces()
        {
            yield return new object[]
            {
                "# Comment 1\n" +
                "Material One;Mat-1;WH-B,5|WH-A,10\n" +
                "\n" +
                "Material Two 2\" -;Mat-2;WH-A,15     "
            };
        }

        [Theory]
        [MemberData(nameof(GetSimpleInput))]
        [MemberData(nameof(GetInputWithComments))]
        [MemberData(nameof(GetInputWithCommentsAndWhitespaces))]
        public void Parse_WithValidText_ReturnsWarehousesCollection_WithOrderNotGuaranteed(
            string inputText)
        {
            //Arrange
            var sut = new DefaultParser(
                new DefaultWarehouseFactory(),
                new DefaultMaterialFactory()
            );

            //Act
            var result = sut.Parse(inputText);

            //Assert
            var orderedResult = result.OrderBy(warehouse => warehouse.Id);

            Assert.Collection(
                orderedResult,
                warehouse =>
                {
                    Assert.Equal("WH-A", warehouse.Id);
                    Assert.Equal(25, warehouse.TotalInStock);
                    var orderedStock = warehouse.GetStock().OrderBy(stockEntry => stockEntry.Key.Id);
                    Assert.Collection(
                        orderedStock,
                        entry =>
                        {
                            Assert.Equal("Mat-1", entry.Key.Id);
                            Assert.Equal("Material One", entry.Key.Name);
                            Assert.Equal(10, entry.Value.Amount);
                        },
                        entry =>
                        {
                            Assert.Equal("Mat-2", entry.Key.Id);
                            Assert.Equal("Material Two 2\" -", entry.Key.Name);
                            Assert.Equal(15, entry.Value.Amount);
                        }
                    );
                },
                warehouse =>
                {
                    Assert.Equal("WH-B", warehouse.Id);
                    Assert.Equal(5, warehouse.TotalInStock);
                    var orderedStock = warehouse.GetStock().OrderBy(stockEntry => stockEntry.Key.Id);
                    Assert.Collection(
                        orderedStock,
                        entry =>
                        {
                            Assert.Equal("Mat-1", entry.Key.Id);
                            Assert.Equal("Material One", entry.Key.Name);
                            Assert.Equal(5, entry.Value.Amount);
                        }
                    );
                }
            );
        }
    }
}

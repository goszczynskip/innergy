using App;
using Core;
using Xunit;

namespace Test
{
    public class DefaultFormatterTest
    {
        [Fact]
        public void
            Format_ReturnsFormattedText_WithSortedWarehousesByStockSizeThenByName_AndWithSortedMaterialsAlphabetically()
        {
            //Arrange
            var sut = new DefaultFormatter();

            var warehouseA = new DefaultWarehouse("WH-A");
            var warehouseB = new DefaultWarehouse("WH-B");
            var warehouseC = new DefaultWarehouse("WH-C");

            var material1 = new DefaultMaterial("Mat-1", "Material 1");
            var material2 = new DefaultMaterial("Mat-2", "Material 2");
            var material3 = new DefaultMaterial("Mat-3", "Material 3");

            warehouseA.AddMaterial(material1, 10);
            warehouseA.AddMaterial(material2, 20);

            warehouseB.AddMaterial(material3, 5);
            warehouseB.AddMaterial(material2, 25);

            warehouseC.AddMaterial(material3, 15);
            warehouseC.AddMaterial(material1, 15);
            warehouseC.AddMaterial(material2, 15);

            var warehouses = new[]
            {
                warehouseB,
                warehouseA,
                warehouseC
            };

            // Act
            var result = sut.Format(warehouses);

            // Assert

            Assert.Equal(
                "WH-C (total 45)\n" +
                "Mat-1: 15\n" +
                "Mat-2: 15\n" +
                "Mat-3: 15\n" +
                "\n" +
                "WH-A (total 30)\n" +
                "Mat-1: 10\n" +
                "Mat-2: 20\n" +
                "\n" +
                "WH-B (total 30)\n" +
                "Mat-2: 25\n" +
                "Mat-3: 5\n" +
                "\n",
                result
            );
        }
    }
}
